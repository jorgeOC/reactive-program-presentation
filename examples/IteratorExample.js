"use strict";

function Iterator(str) {
    const array = str.split("");
    let index = 0;
    return {
        hasNext:() => index < array.length,
        next:() => array[index++],
        toString : str
    }
}

function forEach(iterator, f) {
    while (iterator.hasNext()) f(iterator.next())
}

const iterator = Iterator("latam");// => ['l','a','t','a','n',]

forEach(iterator, (char)=> console.log('char : ' + char));













//
// function map(iterator, f) {
//     let newIterator = '';
//     while (iterator.hasNext()) newIterator +=f(iterator.next());
//     return Iterator(newIterator);
// }
//
// const iterator2 = Iterator("latam");
// const latamUppercase = map(iterator2, (char)=> char.toUpperCase());
//
//
// console.log('latamUppercase : ' + latamUppercase.toString);