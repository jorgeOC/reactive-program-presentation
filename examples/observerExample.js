"use strict";

function Observable() {
    let observersCollection = [];
    return {
        registerObserver:(observer) => observersCollection.push(observer),
        unregisterObserver:(observer) => observersCollection = observersCollection.splice(observer, 1),
        notify :(value) => observersCollection.forEach(observer => observer(value))
    }
}

const observable = new Observable();
let i = 0;
setInterval(()=>observable.notify(++i), 1000);


const observer1 = value => console.log('Desde observer 1 value : ' + value);
// const observer2 = value => console.log('Desde observer 2 value : ' + value);

observable.registerObserver(observer1);
// observable.registerObserver(observer2);




// setTimeout(()=> observable.unregisterObserver(observer1), 5500);
