"use strict";
// https://github.com/ReactiveX/rxjs/blob/master/MIGRATION.md
const Rx = require('rxjs');

// const promise = new Promise((resolve) =>{
//     setTimeout(()=>{
//         console.log('===================================================')
//         resolve("resolveValue")
//     }, 1000);
//
//     console.log("Start Promise")
// });

// promise.then(value => console.log('value : ' + value));


const observable = Rx.Observable.create((observer)=>{
    const idTimeout = setInterval(()=>{
        observer.next("observableValue")
    }, 1000);

    console.log("Start Observable");

    return () =>{
        console.log("Call dispose");
        clearInterval(idTimeout);
    }
});

var subject = new Rx.Subject();

observable.subscribe(subject);



const subscription = subject.subscribe(value => console.log('value : ' + value));
const subscription2 = subject.subscribe(value => console.log('2   value : ' + value));

setTimeout(()=>{
    subscription.unsubscribe();
}, 5000);