"use strict";

const Rx = require('rxjs') ;

function getPromise(deferTime, shouldFail) {
    return Rx.Observable.create((observer)=>{
        const timeoutId = setTimeout(()=>{
            if(shouldFail) {
                observer.error('Eroor after: '+deferTime);
            } else {
                observer.next('Exit after: '+deferTime);
                observer.complete();
            }
        },1000);

        return ()=>{
            clearInterval(timeoutId);
        }
    });
}

var merged = Rx.Observable.merge(getPromise(1), getPromise(2, true).catch(val => Rx.Observable.of({error: val})), getPromise(3));

var subscription = merged.subscribe(
    (nextValue)=> console.log('valor', nextValue),
    (error) => console.log('error', error),
    ()=> console.log(' Cuando se completa!')
);