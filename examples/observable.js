"use strict";
const Rx = require('rxjs') ;


const observable = Rx.Observable.create((observer)=>{
    let i =0;
    setInterval(()=>{
        observer.next(++i);
    },1000);

});

function filterOdd(num) {
    return num % 2 === 0
}

const values = [1,2,3,4,5];

// values
//     .filter(filterOdd)
//     .forEach((value)=> console.log('From array valor:   ', value));




const subscriber = observable
    .filter(filterOdd)
    .map(value => value*10)
    .forEach((nextValue)=> console.log('valor', nextValue));

// setTimeout(()=>subscriber.unsubscribe(),5000);