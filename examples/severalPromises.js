"use strict";

function getPromise(deferTime, shouldFail) {
    return new Promise((resolve, reject)=> {
        setTimeout(()=>{
            if(shouldFail) reject('Eroor after: '+deferTime);
            else resolve('Exit after: '+deferTime)
        },deferTime*1000)
    })
}

Promise.all([getPromise(1), getPromise(2,false), getPromise(3)])
    .then(result => console.log('JSON.stringify(result)', JSON.stringify(result)))
    .catch(error => console.log('Error =  '+error));

