- Hacer npm install

- Para ejecutar cualquier ejemplo, que está dentro de la carpeta example, ejecutar "node example/$nombre_archivo"

- si quieren ejecutar el ejemplo pipedrive, crear una cuenta en pipedrive, y poner dentro del archivo, su usuario y contraseña

- Para ejecutar el ejemplo react, hacer un npm start. Existe un problema en el json que devuelve directamente patiotuerca. Para el ejemplo yo consumí un
  node que a su vez consumía el api "https://ecuador.patiotuerca.com/ptx/api/secure/search?fts=$openTexsearch". Si desean pueden hacer lo mismo o sino cambiar por cualquier otro api get

